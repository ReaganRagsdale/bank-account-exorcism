
public class BankAccount {
	private double balance;
	
	private boolean isOpen = false;
	private int accountNumber;
	
	
	public BankAccount(int accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	public int getAccountNumber() {
		return accountNumber;
	}

	public double getBalance() throws BankAccountActionInvalidException {
		if (!isOpen()) {
			throw new BankAccountActionInvalidException("Account closed");
		}
		return balance;
	}

	public void open() {

		this.balance = 0;
		this.isOpen = true;
	}

	public void deposit(double x) throws BankAccountActionInvalidException {
		if (!isOpen()) {
			throw new BankAccountActionInvalidException("Account closed");
		}
		if (x < 0) {
			throw new BankAccountActionInvalidException("Cannot deposit or withdraw negative amount");
		}
		this.balance += x;
	}

	public void withdraw(double y) throws BankAccountActionInvalidException {
		if (!isOpen()) {
			throw new BankAccountActionInvalidException("Account closed");
		}
		if (this.balance == 0) {
			throw new BankAccountActionInvalidException("Cannot withdraw money from an empty account");
		}
		if (y > this.balance) {
			throw new BankAccountActionInvalidException("Cannot withdraw more money than is currently in the account");
		}
		if (y < 0) {
			throw new BankAccountActionInvalidException("Cannot deposit or withdraw negative amount");
		}
		this.balance -= y;
	}

	public void close() {
		this.isOpen = false;

	}

	public boolean isOpen() {
		return isOpen;
	}


	

}
