import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BankMain {


	private static Scanner input = new Scanner(System.in);
	private static List<BankAccount> bankAccounts = new ArrayList<BankAccount>();

	private static String WorD = "";
	private static BankAccount currentBankAccount = null;

	public static void main(String[] args) throws Exception {

		while (true) {
			System.out.println("Would you like to open a 'new' account or 'view' current accounts?");
			String accountOpen = input.next();

			if (accountOpen.equals("new")) {
				double id = Math.random() * 10000;
				currentBankAccount = new BankAccount((int) id);

				bankAccounts.add(currentBankAccount);
				currentBankAccount.open();
				System.out.println("New Account Number is: " + currentBankAccount.getAccountNumber());
				accountOptions();
			} else if (accountOpen.equals("view")) {
				if( currentBankAccount == null || currentBankAccount.isOpen() == false) {
					System.out.println("There are currently no accounts opened. \nWould you like to open one?");
					String newAccountyes = input.next();
					if(newAccountyes.equals("yes")) {
						double id = Math.random() * 10000;
						currentBankAccount = new BankAccount((int) id);

						bankAccounts.add(currentBankAccount);
						currentBankAccount.open();
						System.out.println("New Account Number is: " + currentBankAccount.getAccountNumber());
						accountOptions();
					}
					else if(newAccountyes.equals("no")) {
						System.out.println("You are no use to us, Goodbye.");
						System.exit(0);
					}
					else if (!newAccountyes.equals("yes") && !newAccountyes.equals("no")) {
						System.out.println("Please enter yes or no");
						break;
					}
					
				};
				int switchToAccnt;
				for (int z = 0; z < bankAccounts.size(); z++) {

					BankAccount account = bankAccounts.get(z);
					System.out.println("Account Number: " + account.getAccountNumber() + " Your balance is: "
							+ account.getBalance());

				}
				System.out.println("Please enter the account number you wish to enter or press '1' to create a new account.");
				switchToAccnt = input.nextInt();
				if (switchToAccnt == 1) {
					double id = Math.random() * 10000;
					currentBankAccount = new BankAccount((int) id);

					bankAccounts.add(currentBankAccount);
					currentBankAccount.open();
					System.out.println("New Account Number is: " + currentBankAccount.getAccountNumber());
					accountOptions();
				}
				else currentBankAccount = findBankAccount(bankAccounts, switchToAccnt);
				accountOptions();
				

			}
			else if (!accountOpen.equals("new") && !accountOpen.equals("view")) {
				System.out.println("Please enter new or view");
			}

		}
	}

	private static BankAccount findBankAccount(List<BankAccount> bankAccounts, int accountNumber) {
		for (BankAccount ba : bankAccounts) {
			if (accountNumber == ba.getAccountNumber()) {
				return ba;
			}
		}
		return null;

	}

	private static void accountOptions() {
		System.out.println("Currently in Account Number: " + currentBankAccount.getAccountNumber());
		while (true) {

			System.out.println(
					"Would you like to 'deposit', 'withdraw' money, check your 'balance', 'close' an account, 'switch' accounts or 'exit'?");
			WorD = input.next();
			if (WorD.equals("withdraw")) {
				System.out.println("Please enter the dollar amount you would you like to withdraw.");
				double withAmnt = input.nextDouble();
				try {
					currentBankAccount.withdraw(withAmnt);
					System.out.println("Here is your money, your balance is now $" + currentBankAccount.getBalance());
				} catch (BankAccountActionInvalidException e) {

					e.printStackTrace();
				}
			}
			else if (WorD.equals("deposit")) {
				System.out.println("Please enter the dollar amount you would you like to deposit.");
				double depAmnt = input.nextDouble();
				try {
					currentBankAccount.deposit(depAmnt);
					System.out.println("Thank you, your balance is now $" + currentBankAccount.getBalance());
				} catch (BankAccountActionInvalidException e) {

					e.printStackTrace();
				}
			}
			else if (WorD.equals("balance")) {

				try {
					currentBankAccount.getBalance();
					System.out.println("Your balance is $" + currentBankAccount.getBalance());
				} catch (BankAccountActionInvalidException e) {

					e.printStackTrace();
				}
			}
			else if (WorD.equals("close")) {

				bankAccounts.remove(currentBankAccount);
				System.out.println("Account " + currentBankAccount.getAccountNumber() + ": closed");
				break;

			}
			else if (WorD.equals("exit")) {
				System.exit(0);
			}
			else if (WorD.equals("switch")) {

				break;

			}
			else if (!WorD.equals("withdraw") && !WorD.equals("deposit") && !WorD.equals("balance") && !WorD.equals("close")
					&& !WorD.equals("exit") && !WorD.equals("switch")) {
				System.out.println(" ");
				System.out.println("Please select one of the options.");

			}
		}

	}
}
